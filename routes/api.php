<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('login', 'AuthController@login');


Route::group(["middleware" => ["auth:api", "transactioned"]], function(){
    Route::put('change-password', 'AuthController@changePassword');
    
    Route::resource('users', 'UserController');
    Route::resource('accesses', 'AccessController');
    Route::resource('profiles', 'ProfileController');
    Route::resource('departments', 'DepartmentController');
    Route::resource('positions', 'PositionController');

    Route::group(['prefix' => 'dashboard'], function(){
        Route::get('users-by-department', 'DashboardController@usersByDepartment');
        Route::get('users-by-position', 'DashboardController@usersByPosition');
        Route::get('users-by-profile', 'DashboardController@usersByProfile');
    });
});

Route::any('/{any}', function(){
    return response("Not Found", 404);
});