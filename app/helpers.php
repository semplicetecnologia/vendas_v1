<?php
function secondsToTimeString($maxSeconds)
{
    $seconds = $maxSeconds % 60;
    $seconds = str_pad($seconds, 2, "0", STR_PAD_LEFT);

    $minutes = intdiv($maxSeconds, 60) % 60;
    $minutes = str_pad($minutes, 2, "0", STR_PAD_LEFT);

    $hours = intdiv(intdiv($maxSeconds, 60), 60) % 60;
    $hours = str_pad($hours, 2, "0", STR_PAD_LEFT);

    $timeString = "$hours:$minutes:$seconds";

    return $timeString;
}

function removeSpecialCharacters($string)
{
    return preg_replace('/[^0-9]/', '', $string);
}

function BRL2Float($string)
{
    if(empty($string)) return null;
    
    $string = str_replace('.', '', $string);
    $string = str_replace(',', '.', $string);

    return (float) $string;
}