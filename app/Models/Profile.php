<?php

namespace App\Models;

use App\Models\Access;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Profile extends Model
{
    use SoftDeletes;
    
    protected $guarded = [];

    public function accesses()
    {
        return $this->belongsToMany(Access::class);
    }

}
