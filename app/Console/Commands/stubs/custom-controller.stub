<?php

namespace App\Http\Controllers;

use App\Models\{{ model }};
use Illuminate\Http\Request;
use App\Http\Resources\{{ model }}Resource;

class {{ model }}Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $perPage = $request->input('itemsPerPage', -1) == -1 ? {{ model }}::count() : $request->input('itemsPerPage');
        $search = $request->input('search', null);

        $query = {{ model }}::query();

        if($search) {
            $query->where('name', 'like', "%$search%");
        }

        // Sort
        $sortBy = $request->sortBy ?? [];
        foreach($sortBy as $key => $field) {
            $desc = $request->sortDesc[$key] == "true" ? 'desc' : 'asc';
            $query->orderBy($field, $desc);
        }

        return {{ model }}Resource::collection( $query->paginate( $perPage ) );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        ${{ model_variable }} = {{ model }}::create($request->all());

        return new {{ model }}Resource(${{ model_variable }});
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show({{ model }} ${{ model_variable }})
    {
        return new {{ model }}Resource(${{ model_variable }});
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, {{ model }} ${{ model_variable }})
    {
        ${{ model_variable }}->update($request->all());

        return new {{ model }}Resource(${{ model_variable }});
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy({{ model }} ${{ model_variable }})
    {
        ${{ model_variable }}->delete();

        return new {{ model }}Resource(${{ model_variable }});
    }
}
