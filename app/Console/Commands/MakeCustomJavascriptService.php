<?php
namespace App\Console\Commands;

use Illuminate\Support\Str;
use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputArgument;

class MakeCustomJavascriptService extends GeneratorCommand
{
    /**
     * O nome e a assinatura do comando do console.
     *
     * @var string
     */
    protected $name = 'semplice:service';

    /**
     * A descrição do comando do console.
     *
     * @var string
     */
    protected $description = 'Create a new service class with customized properties to be utilized in frontend';

    /**
     * O tipo de classe sendo gerada.
     *
     * @var string
     */
    protected $type = 'Service';

     /**
     * Substitui o nome da classe para o stub fornecido.
     *
     * @param  string  $stub
     * @param  string  $name
     * @return string
     */
    protected function replaceClass($stub, $name)
    {
        $stub = parent::replaceClass($stub, $name);

        $name_argument = $this->argument('name');
        $name_plural = Str::snake(Str::pluralStudly(class_basename($name_argument)));

        $stub = str_replace('{{ model_variable }}', strtolower($name_argument), $stub);
        $stub = str_replace('{{ model_plural }}', $name_plural, $stub);

        return str_replace('{{ model }}', $this->argument('name'), $stub);
    }

    /**
     * Obtem o arquivo stub para o gerador.
     *
     * @return string
     */
    protected function getStub()
    {
        return  app_path() . '/Console/Commands/stubs/custom-javascript-service.stub';
    }

    /**
     * Obtém o namespace padrão para a classe.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace.'\..\resources\js\services';
    }

    /**
     * Get the destination class path.
     *
     * @param  string  $name
     * @return string
     */
    protected function getPath($name)
    {
        $name = Str::replaceFirst($this->rootNamespace(), '', $name);

        return $this->laravel['path'].'/'.str_replace('\\', '/', $name).'Service.js';
    }

    /**
     * Obtém os argumentos do comando do console.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['name', InputArgument::REQUIRED, 'The name of the model.'],
        ];
    }
}