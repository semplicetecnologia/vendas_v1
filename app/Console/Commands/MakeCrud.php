<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class MakeCrud extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'semplice:crud {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create classes model, resource, controller for the given model name';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Create a model.
     *
     * @return void
     */
    protected function createModel()
    {
        $name = $this->argument('name');

        $this->call('semplice:model', [
            'name' => "{$name}",
        ]);
    }

    /**
     * Create a model resource for the model.
     *
     * @return void
     */
    protected function createResource()
    {
        $name = $this->argument('name');

        $this->call('make:resource', [
            'name' => "{$name}Resource",
        ]);
    }    

    /**
     * Create a resource controller for the model.
     *
     * @return void
     */
    protected function createResourceController()
    {
        $name = $this->argument('name');

        $this->call('semplice:controller', [
            'name' => "{$name}Controller",
        ]);
    }

    /**
     * Create a service for api consume for the model
     *
     * @return void
     */
    protected function createJavascriptService()
    {
        $name = $this->argument('name');

        $this->call('semplice:service', [
            'name' => "{$name}",
        ]);
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->createModel();
        $this->createResource();
        $this->createResourceController();
        $this->createJavascriptService();
    }
}
