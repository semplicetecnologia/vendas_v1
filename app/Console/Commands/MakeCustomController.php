<?php
namespace App\Console\Commands;

use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputArgument;

class MakeCustomController extends GeneratorCommand
{
    /**
     * O nome e a assinatura do comando do console.
     *
     * @var string
     */
    protected $name = 'semplice:controller';

    /**
     * A descrição do comando do console.
     *
     * @var string
     */
    protected $description = 'Create a new controller class with customized properties and methods';

    /**
     * O tipo de classe sendo gerada.
     *
     * @var string
     */
    protected $type = 'Controller';
   
     /**
     * Substitui o nome da classe para o stub fornecido.
     *
     * @param  string  $stub
     * @param  string  $name
     * @return string
     */
    protected function replaceClass($stub, $name)
    {
        $stub = parent::replaceClass($stub, $name);

        $name_argument = str_replace('Controller', '', $this->argument('name'));

        $stub = str_replace('{{ model }}', $name_argument, $stub);
        $stub = str_replace('{{ model_variable }}', strtolower($name_argument), $stub);

        return $stub;
    }

    /**
     * Obtem o arquivo stub para o gerador.
     *
     * @return string
     */
    protected function getStub()
    {
        return  app_path() . '/Console/Commands/stubs/custom-controller.stub';
    }

    /**
     * Obtém o namespace padrão para a classe.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace . '\Http\Controllers';
    }

    /**
     * Obtém os argumentos do comando do console.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['name', InputArgument::REQUIRED, 'The name of the controller.'],
        ];
    }
}