<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Profile;
use App\Models\Position;
use App\Models\Department;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function usersByDepartment()
    {
        $departments = Department::orderBy('name')->get();

        $result = [];
        foreach($departments as $department) 
        {
            $result[] = [
                'name' => $department->name,
                'total' => User::where('department_id', $department->id)->count()
            ];
        }

        return $result;
    }

    public function usersByPosition()
    {
        $positions = Position::orderBy('name')->get();

        $result = [];
        foreach($positions as $position) 
        {
            $result[] = [
                'name' => $position->name,
                'total' => User::where('position_id', $position->id)->count()
            ];
        }

        return $result;
    }

    public function usersByProfile()
    {
        $profiles = Profile::orderBy('name')->get();

        $result = [];
        foreach($profiles as $profile) 
        {
            $result[] = [
                'name' => $profile->name,
                'total' => User::where('profile_id', $profile->id)->count()
            ];
        }

        return $result;
    }
}
