<?php

namespace App\Http\Controllers;

use App\Models\Department;
use Illuminate\Http\Request;
use App\Http\Resources\DepartmentResource;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $perPage = $request->input('itemsPerPage', -1) == -1 ? Department::count() : $request->input('itemsPerPage');
        $search = $request->input('search', null);

        $query = Department::query();

        if($search) {
            $query->where('name', 'like', "%$search%");
        }

        // Sort
        $sortBy = $request->sortBy ?? [];
        foreach($sortBy as $key => $field) {
            $desc = $request->sortDesc[$key] == "true" ? 'desc' : 'asc';
            $query->orderBy($field, $desc);
        }

        return DepartmentResource::collection( $query->paginate( $perPage ) );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $department = Department::create($request->all());

        return new DepartmentResource($department);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Department $department)
    {
        return new DepartmentResource($department);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Department $department)
    {
        $department->update($request->all());

        return new DepartmentResource($department);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Department $department)
    {
        $department->delete();

        return new DepartmentResource($department);
    }
}
