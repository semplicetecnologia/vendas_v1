<?php

namespace App\Http\Controllers;

use App\Models\Access;
use Illuminate\Http\Request;
use App\Http\Resources\AccessResource;

class AccessController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $perPage = $request->input('itemsPerPage', -1) == -1 ? Access::count() : $request->input('itemsPerPage');
        $search = $request->input('search', null);

        $query = Access::query();

        if($search) {
            $query->where('name', 'like', "%$search%");
        }

        // Sort
        $sortBy = $request->sortBy ?? [];
        foreach($sortBy as $key => $field) {
            $desc = $request->sortDesc[$key] == "true" ? 'desc' : 'asc';
            $query->orderBy($field, $desc);
        }

        return AccessResource::collection( $query->paginate( $perPage ) );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $access = Access::create($request->all());

        return new AccessResource($access);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Access $access)
    {
        return new AccessResource($access);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Access $access)
    {
        $access->update($request->all());

        return new AccessResource($access);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Access $access)
    {
        $access->delete();

        return new AccessResource($access);
    }
}
