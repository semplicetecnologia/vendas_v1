<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailUserCredentials;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $perPage = $request->input('itemsPerPage', -1) == -1 ? User::count() : $request->input('itemsPerPage');
        $search = $request->input('search', null);

        $query = User::query();
        $query->select('users.*');
        $query->leftJoin('departments as department', 'users.department_id', 'department.id');
        $query->leftJoin('positions as position', 'users.position_id', 'position.id');
        $query->leftJoin('profiles as profile', 'users.profile_id', 'profile.id');

        if($search) {
            $query->where('name', 'like', "%$search%");
        }

        // Sort
        $sortBy = $request->sortBy ?? [];
        foreach($sortBy as $key => $field) {
            $desc = $request->sortDesc[$key] == "true" ? 'desc' : 'asc';
            $query->orderBy($field, $desc);
        }

        $query->with('department', 'position', 'profile');
        
        return UserResource::collection( $query->paginate( $perPage ) );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request = $request->all();
        $request['password'] = Str::random(8);
        $user = User::create($request);

        // $user->password = Str::random(8);
        // $user->token_reset = Str::random(100);
        // $user->token_reset_created_at = Carbon::now()->addHours(48);

        // if( 1 ) {
        //     Mail::to($user->email)->send(new SendMailUserCredentials($user->name, $user->email, $user->token_reset));
        // } else {
        //     return response("Erro ao criar usuário", 400);
        // }

        return new UserResource($user);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return new UserResource($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $user->update($request->all());

        return new UserResource($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();

        return new UserResource($user);
    }
}
