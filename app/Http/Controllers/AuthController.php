<?php

namespace App\Http\Controllers;

use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
            $user = Auth::user();
            $token = $user->createToken('token')->accessToken;
            
            return response()->json(["user" => $user, "token" => $token], 200);
        } else {
            $registered = User::where('email', request('email'))->exists();
            $message = !$registered ? "E-mail não cadastrado." : "Senha incorreta.";

            return response()->json(['errors' => $message], 403);
        }
    }

    public function changePassword(Request $request)
    {
        $user = Auth::user();
        $current_password = $request->password;
        $new_password = $request->new_password;
        
        if( Hash::check($current_password, $user->password) ) {
            $user->password = Hash::make($new_password);
            $user->save();

            return response("Senha alterada com sucesso", 200);
        }
        else {
            return response("Senha atual incorreta", 422);
        }
    }
}
