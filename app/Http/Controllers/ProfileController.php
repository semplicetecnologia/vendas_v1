<?php

namespace App\Http\Controllers;

use App\Models\Profile;
use Illuminate\Http\Request;
use App\Http\Resources\ProfileResource;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $perPage = $request->input('itemsPerPage', -1) == -1 ? Profile::count() : $request->input('itemsPerPage');
        $search = $request->input('search', null);

        $query = Profile::query();

        if($search) {
            $query->where('name', 'like', "%$search%");
        }

        // Sort
        $sortBy = $request->sortBy ?? [];
        foreach($sortBy as $key => $field) {
            $desc = $request->sortDesc[$key] == "true" ? 'desc' : 'asc';
            $query->orderBy($field, $desc);
        }

        return ProfileResource::collection( $query->paginate( $perPage ) );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $profile = Profile::create($request->only('name'));
        
        $ids = collect($request->accesses)->pluck('id');
        $profile->accesses()->sync($ids);

        return new ProfileResource($profile);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $profile = Profile::with('accesses')->findOrFail($id);

        return new ProfileResource($profile);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Profile $profile)
    {
        $profile->update($request->only('name'));

        $ids = collect($request->accesses)->pluck('id');
        $profile->accesses()->sync($ids);

        return new ProfileResource($profile);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profile $profile)
    {
        $profile->delete();

        return new ProfileResource($profile);
    }
}
