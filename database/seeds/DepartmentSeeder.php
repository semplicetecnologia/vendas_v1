<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if( !DB::table('departments')->where('name', 'Fiscal')->exists() ){
            DB::table('departments')->insert([
                'name' => "Fiscal",                
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }

        if( !DB::table('departments')->where('name', 'Contábil')->exists() ){
            DB::table('departments')->insert([
                'name' => "Contábil",                
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }

        if( !DB::table('departments')->where('name', 'Financeira')->exists() ){
            DB::table('departments')->insert([
                'name' => "Financeira",                
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }

        if( !DB::table('departments')->where('name', 'Comercial')->exists() ){
            DB::table('departments')->insert([
                'name' => "Comercial",                
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }
    }
}
