<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class PositionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if( !DB::table('positions')->where('name', 'Diretor')->exists() ){
            DB::table('positions')->insert([
                'name' => "Diretor",                
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }

        if( !DB::table('positions')->where('name', 'Gestor')->exists() ){
            DB::table('positions')->insert([
                'name' => "Gestor",                
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }

        if( !DB::table('positions')->where('name', 'Analista')->exists() ){
            DB::table('positions')->insert([
                'name' => "Analista",                
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }
    }
}
