<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class AccessSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $COLLA_MODULE = DB::table('modules')->where('ref', 'COLLA')->first();

        $this->add($COLLA_MODULE->id, 'USER.CRUD', 'Gerenciar usuários');
        $this->add($COLLA_MODULE->id, 'DEPARTMENT.CRUD', 'Gerenciar departamentos');
        $this->add($COLLA_MODULE->id, 'POSITION.CRUD', 'Gerenciar cargos');
        $this->add($COLLA_MODULE->id, 'PROFILE.CRUD', 'Gerenciar perfis');
    }

    private function add($module_id, $ref, $feature)
    {
        if(!DB::table('accesses')->where('ref', $ref)->exists() ){
            DB::table('accesses')->insert([
                'ref' => $ref,
                'feature' => $feature,
                'module_id' => $module_id,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }
    }
}
