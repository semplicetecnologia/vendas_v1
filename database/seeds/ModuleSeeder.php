<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class ModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if( !DB::table('modules')->where('ref', 'COLLA')->exists() ){
            DB::table('modules')->insert([
                'ref' => "COLLA",
                'name' => "Módulo de colaboradores",                
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }
    }
}
