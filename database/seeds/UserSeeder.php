<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {       
        if( DB::table('users')->count() == 0 ) {
            DB::table('users')->insert([
                'name' => "Semplice User",
                'email' => "semplice@semplice.net.br",
                'password' => Hash::make('semplice'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }
    }
}
