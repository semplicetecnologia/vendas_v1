@extends('emails.layout')

@section('content')
    <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="body">
        <tr>
            <td>&nbsp;</td>
            <td class="container">
                <div class="content">

                    <!-- START CENTERED WHITE CONTAINER -->
                    <table role="presentation" class="main">

                        <!-- START MAIN CONTENT AREA -->
                        <tr>
                            <td class="wrapper">
                                <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <p style="text-align: center;">
                                                <img src="https://i.imgur.com/wZZtlkc.png">
                                            </p>
                                            <p>Caro, {{$name}}</a> </p>
                                            <p style="text-align: center;">
                                                Informamos que um novo cadastro foi criado para seu e-mail, defina uma nova senha de acesso clicando no link abaixo
                                            </p>
                                            <a href="{{url('/')}}/reset-password/{{$token}}">Clique Aqui para definir sua senha de acesso</a>
                                            <p>
                                            <b>E-mail: </b> {{$email}}<br>
                                            <b>Link de Acesso: </b> <a href="{{url('/')}}">{{url('/')}}</a>
                                            </p>
                                            <br><br><br>
                                            <p>A Goodman agradece sua atenção.</p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <!-- END MAIN CONTENT AREA -->
                    </table>
                    <!-- END CENTERED WHITE CONTAINER -->

                    <!-- START FOOTER -->
                    <div class="footer">
                        <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="content-block">
                                    <span class="apple-link">Goodman - 2021</span>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <!-- END FOOTER -->

                </div>
            </td>
            <td>&nbsp;</td>
        </tr>
    </table>
@endsection