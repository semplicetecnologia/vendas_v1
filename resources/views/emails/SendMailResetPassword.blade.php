@extends('emails.layout')

@section('content')
    <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="body">
        <tr>
            <td>&nbsp;</td>
            <td class="container">
                <div class="content">

                    <!-- START CENTERED WHITE CONTAINER -->
                    <table role="presentation" class="main">

                        <!-- START MAIN CONTENT AREA -->
                        <tr>
                            <td class="wrapper">
                                <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <p style="text-align: center;">
                                                <img src="https://i.imgur.com/wZZtlkc.png">
                                            </p>
                                            <p>Olá, <b>{{$name}}</b></a> </p>
                                            <p style="text-align: left;">
                                                Um pedido de recuperação de senha foi iniciado para sua conta. Para definir uma nova senha de acesso, clique no link a seguir: 
                                                <br><br><br>
                                                <a href="{{url('/')}}/reset-password/{{$token}}">{{url('/')}}/reset-password/{{$token}}</a>
                                                <br><br>
                                                O link acima expira automaticamente após 2h depois de gerado. Se você não solicitou a recuperação de senha, por favor ignore esse e-mail.
                                            </p>
                                            <p style="text-align: left;">
                                                <br><br>
                                                Equipe Goodman
                                            </p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <!-- END MAIN CONTENT AREA -->
                    </table>
                    <!-- END CENTERED WHITE CONTAINER -->

                    <!-- START FOOTER -->
                    <div class="footer">
                        <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="content-block">
                                    <span class="apple-link">Goodman - 2021</span>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <!-- END FOOTER -->

                </div>
            </td>
            <td>&nbsp;</td>
        </tr>
    </table>
@endsection