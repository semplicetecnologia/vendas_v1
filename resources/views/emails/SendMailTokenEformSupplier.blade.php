@extends('emails.layout')

@section('content')
    <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="body">
        <tr>
            <td>&nbsp;</td>
            <td class="container">
                <div class="content">

                    <!-- START CENTERED WHITE CONTAINER -->
                    <table role="presentation" class="main">

                        <!-- START MAIN CONTENT AREA -->
                        <tr>
                            <td class="wrapper">
                                <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <p style="text-align: center;">
                                                <img src="https://i.imgur.com/wZZtlkc.png">
                                            </p>
                                            <p>Olá, {{$name}}</a> </p>
                                            <p style="text-align: center;">
                                                Caro fornecedor, <br> 
                                                Seja bem vindo ao processo de cadastro da GOODMAN Brasil. Acesse o link abaixo para dar continuidade. É um prazer poder tê-los cadastrados em nossa base de fornecedores. Para uma melhor experiência, aconselhamos abrir o Link através do Google Chrome. Entretanto, se preferir, você pode usar qualquer um dos principais navegadores (Firefox, Explorer, Safari) desde que estejam atualizados. 
                                                <br>Fale com a GOODMAN: Envio de Nota Fiscal: nfe@goodman.com | Assuntos de cobrança:  e dúvidas relacionadas: financeirobr@goodman.com. A GOODMAN Brasil agradece a sua atenção.
                                            </p>
                                            <table role="presentation" border="0" cellpadding="0" cellspacing="0"
                                                   class="btn btn-primary">
                                                <tbody>
                                                <tr>
                                                    <td align="center">
                                                        <table role="presentation" border="0" cellpadding="0"
                                                               cellspacing="0">
                                                            <tbody>
                                                            <tr>
                                                                <td>
                                                                    <center>
                                                                        <a href="{{url('/formulario-fornecedor').'/'.$token}}"
                                                                           target="_blank">ABRIR FORMULÁRIO</a></a>
                                                                    </center>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <!-- END MAIN CONTENT AREA -->
                    </table>
                    <!-- END CENTERED WHITE CONTAINER -->

                    <!-- START FOOTER -->
                    <div class="footer">
                        <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="content-block">
                                    <span class="apple-link">Goodman - 2021</span>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <!-- END FOOTER -->

                </div>
            </td>
            <td>&nbsp;</td>
        </tr>
    </table>
@endsection