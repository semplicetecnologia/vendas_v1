import Vue from "vue";
import Router from "vue-router";
import { isAuthenticated } from "@/utils/AuthUtils";
import { hasPermission } from "@/utils/PermissionUtils";

Vue.use(Router);

const router = new Router({
    mode: "history",
    routes: [
        {
            path: "/",
            redirect: { name: "login" }
        },
        {
            path: "/login",
            name: "login",
            component: () => import("@/views/auth/Login")
        },
        {
            path: "/recover-password",
            name: "recover-password",
            component: () => import("@/views/auth/RecoverPassword")
        },
        {
            path: "/test",
            component: () => import("@/views/auth/RecoverPassword")
        },
        {
            path: "/system",
            name: "system",
            component: () => import("@/views/system/System"),
            children: [
                {
                    path: "config",
                    name: "config",
                    component: () => import("@/views/config/Config"),
                    meta: {
                        requiresAuth: true
                    }
                },
                {
                    path: "dashboard",
                    name: "dashboard",
                    component: () => import("@/views/dashboard/Dashboard"),
                    meta: {
                        requiresAuth: true
                        // access: "DASH"
                    }
                },
                {
                    path: "users",
                    name: "users",
                    component: () => import("@/views/user/Users"),
                    meta: {
                        requiresAuth: true,
                        access: "USER.CRUD"
                    }
                },
                {
                    path: "users/:id",
                    name: "user-edit",
                    component: () => import("@/views/user/UserForm"),
                    meta: {
                        requiresAuth: true,
                        access: "USER.CRUD"
                    }
                },
                {
                    path: "departments",
                    name: "departments",
                    component: () => import("@/views/department/Departments"),
                    meta: {
                        requiresAuth: true,
                        access: "DEPARTMENT.CRUD"
                    }
                },
                {
                    path: "departments/:id",
                    name: "departments-edit",
                    component: () =>
                        import("@/views/department/DepartmentForm"),
                    meta: {
                        requiresAuth: true,
                        access: "DEPARTMENT.CRUD"
                    }
                },
                {
                    path: "positions",
                    name: "positions",
                    component: () => import("@/views/position/Positions"),
                    meta: {
                        requiresAuth: true,
                        access: "POSITION.CRUD"
                    }
                },
                {
                    path: "positions/:id",
                    name: "positions-edit",
                    component: () => import("@/views/position/PositionForm"),
                    meta: {
                        requiresAuth: true,
                        access: "POSITION.CRUD"
                    }
                },
                {
                    path: "profiles",
                    name: "profiles",
                    component: () => import("@/views/profile/Profiles"),
                    meta: {
                        requiresAuth: true,
                        access: "PROFILE.CRUD"
                    }
                },
                {
                    path: "profiles/:id",
                    name: "profiles-edit",
                    component: () => import("@/views/profile/ProfileForm"),
                    meta: {
                        requiresAuth: true,
                        access: "PROFILE.CRUD"
                    }
                },
                {
                    path: "permission-denied",
                    name: "system.permission-denied",
                    component: () => import("@/views/PermissionDenied")
                },
                {
                    path: "*",
                    name: "system.404",
                    component: () => import("@/views/NotFound")
                }
            ]
        },
        {
            path: "/permission-denied",
            name: "permission-denied",
            component: () => import("@/views/PermissionDenied")
        },
        {
            path: "*",
            name: "404",
            component: () => import("@/views/NotFound")
        }
    ]
});

router.beforeEach((to, from, next) => {
    if (to.name == "login" && isAuthenticated()) next("/system");
    else if (to.meta.requiresAuth && !isAuthenticated()) {
        next("/login");
    } else if (to.meta.access != undefined && !hasPermission(to.meta.access)) {
        if (to.path.includes("system"))
            next({ name: "system.permission-denied" });
        else next({ name: "permission-denied" });
    } else {
        next();
    }
});

export default router;
