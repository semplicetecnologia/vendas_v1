import ApiService from "./ApiService";

const api_service = new ApiService();

const url = "departments";

class DepartmentService {
    static async all() {
        const response = await api_service.get(url);
        return response.data;
    }

    static paginate(options) {
        return api_service.get(url, { ...options });
    }

    static async get(id) {
        const record = await api_service.get(`${url}/${id}`);
        return record.data;
    }

    static async create(department) {
        const new_record = await api_service.post(url, department);
        return new_record.data;
    }

    static async edit(department) {
        const edited_record = await api_service.put(
            `${url}/${department.id}`,
            department
        );
        return edited_record.data;
    }

    static async delete(id) {
        const deleted_record = await api_service.delete(`${url}/${id}`);
        return deleted_record.data;
    }
}

export default DepartmentService;
