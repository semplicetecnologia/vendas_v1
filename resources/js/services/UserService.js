import ApiService from "./ApiService";

const api_service = new ApiService();

const url = "users";

class UserService {
    static async all() {
        const response = await api_service.get(url);
        return response.data;
    }

    static paginate(options) {
        return api_service.get(url, { ...options });
    }

    static async get(id) {
        const record = await api_service.get(`${url}/${id}`);
        return record.data;
    }

    static async create(user) {
        const new_user = await api_service.post(url, user);
        return new_user.data;
    }

    static async edit(user) {
        const edited_user = await api_service.put(`${url}/${user.id}`, user);
        return edited_user.data;
    }

    static async delete(id) {
        const deleted_user = await api_service.delete(`${url}/${id}`);
        return deleted_user.data;
    }
}

export default UserService;
