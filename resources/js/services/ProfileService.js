import ApiService from "./ApiService";

const api_service = new ApiService();

const url = "profiles";

class ProfileService {
    static async all() {
        const response = await api_service.get(url);
        return response.data;
    }

    static paginate(options) {
        return api_service.get(url, { ...options });
    }

    static async get(id) {
        const record = await api_service.get(`${url}/${id}`);
        return record.data;
    }

    static async create(profile) {
        const new_record = await api_service.post(url, profile);
        return new_record.data;
    }

    static async edit(profile) {
        const edited_record = await api_service.put(
            `${url}/${profile.id}`,
            profile
        );
        return edited_record.data;
    }

    static async delete(id) {
        const deleted_record = await api_service.delete(`${url}/${id}`);
        return deleted_record.data;
    }
}

export default ProfileService;
