import ApiService from "./ApiService";

const api_service = new ApiService();

class DashboardService {
    static async usersByDepartment() {
        const response = await api_service.get("dashboard/users-by-department");

        return response;
    }

    static async usersByPosition() {
        const response = await api_service.get("dashboard/users-by-position");

        return response;
    }

    static async usersByProfile() {
        const response = await api_service.get("dashboard/users-by-profile");

        return response;
    }
}

export default DashboardService;
