import ApiService from "./ApiService";

const api_service = new ApiService();

const url = "accesses";

class AccessService {
    static async all() {
        const response = await api_service.get(url);
        return response.data;
    }

    static paginate(options) {
        return api_service.get(url, { ...options });
    }

    static async get(id) {
        const record = await api_service.get(`${url}/${id}`);
        return record.data;
    }

    static async create(access) {
        const new_record = await api_service.post(url, access);
        return new_record.data;
    }

    static async edit(access) {
        const edited_record = await api_service.put(`${url}/${access.id}`, access);
        return edited_record.data;
    }

    static async delete(id) {
        const deleted_record = await api_service.delete(`${url}/${id}`);
        return deleted_record.data;
    }
}

export default AccessService;
