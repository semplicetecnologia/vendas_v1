import ApiService from "./ApiService";

const api_service = new ApiService();

class AuthService {
    static login(email, password) {
        return api_service.post("login", { email: email, password: password });
    }

    static async resetPassword() {}

    static async loggedUser() {
        const user = await api_service.get("user");
        return user;
    }

    static async changePassword(current_password, new_password) {
        return await api_service.put("change-password", {
            password: current_password,
            new_password: new_password
        });
    }
}

export default AuthService;
