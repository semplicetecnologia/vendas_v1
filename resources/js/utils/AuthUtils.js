import store from "@/store";

const isAuthenticated = () => {
    if (
        store.state.token == "" ||
        store.state.token == null ||
        store.state.token == undefined
    )
        return false;

    return true;
};

export { isAuthenticated };
