const isEmpty = variable => {
    if (variable == undefined || variable == null) return true;

    return false;
};

function getRandomBool() {
    return Math.random();
}

function getRandomFloat(min, max) {
    return Math.random() * (max - min) + min;
}

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
}

export { isEmpty, getRandomBool, getRandomFloat, getRandomInt };
