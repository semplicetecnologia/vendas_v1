export function required() {
    return v => !!v || `Campo é obrigatório`;
}

export function email() {
    return v => /\S+@\S+\.\S+/.test(v) || `E-mail deve ser válido`;
}

export function maxLength(maxLength) {
    return v => (v || '').length <= maxLength || `Limite de ${maxLength} caracteres`;
}

export function minLength(minLength) {
    return v => (v || '').length >= minLength || `Mínimo de ${minLength} caracteres`;
}

export function maxInt(max) {
    return v => parseInt(v || 0) <= max || `Valor máximo de ${max}`;
}

export function minInt(min) {
    return v => parseInt(v || 0) >= min || `Valor mínimo de ${min}`;
}

export function maxFloat(max) {
    return v => parseFloat(v || 0) <= max || `Valor máximo de ${max}`;
}

export function minFloat(min) {
    return v => parseFloat(v || 0) >= min || `Valor mínimo de ${min}`;
}

export function validDate() {
    return v => {
        const dateString = (v || '');
        const [day, month, year] = dateString.split("/");
        let date = new Date(year, month - 1, day);
        const valid = (date.getDate() == day && date.getMonth() == month - 1 && date.getFullYear() == year);

        if ((dateString.length < 10) || !valid) {
            return "Data inválida";
        } else {
            return true;
        }
    }
}

export function validTime() {
    return v => {
        if ((v || '').length == 0) return true;
        const hour = (v || '').substring(0, 2);
        const minutes = (v || '').substring(3, 5);
        if (parseInt(hour) > 23 || parseInt(minutes) > 60 || (v || '').length != 5) {
            return "Horário inválido";
        } else {
            return true;
        }
    }
}
