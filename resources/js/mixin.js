import Vue from "vue";
import { hasPermission } from "@/utils/PermissionUtils";
import { isEmpty } from "@/utils/MiscUtils";

Vue.mixin({
    methods: {
        hasPermission: hasPermission,
        isEmpty: isEmpty,
        usDate: function(date) {
            return date
                .split("/")
                .reverse()
                .join("-");
        },
        brDate: function(date) {
            return date
                .split("-")
                .reverse()
                .join("/");
        }
    }
});
