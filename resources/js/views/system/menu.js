const menu = [
    {
        title: "Dashboard",
        route: "/system/dashboard",
        icon: "mdi-chart-bar"
        // access: "DASH"
    },
    {
        title: "Colaboradores",
        icon: "mdi-account-cog",
        children: [
            {
                title: "Usuários",
                route: "/system/users",
                icon: "mdi-account-group",
                access: "USER.CRUD"
            },
            {
                title: "Perfis",
                route: "/system/profiles",
                icon: "mdi-account-key",
                access: "PROFILE.CRUD"
            },
            {
                title: "Departamentos",
                route: "/system/departments",
                icon: "mdi-sitemap",
                access: "DEPARTMENT.CRUD"
            },
            {
                title: "Cargos",
                route: "/system/positions",
                icon: "mdi-clipboard-account",
                access: "POSITION.CRUD"
            }
        ]
    }
];

export default menu;
